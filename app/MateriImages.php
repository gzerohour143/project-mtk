<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MateriImages extends Model
{
    protected $fillable = [
        'materi_id', 'image'
    ];


    public function materi_images()
    {
        return $this->belongsTo(Materi::class, 'materi_id', 'id');
    }

}

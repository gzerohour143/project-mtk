<?php

namespace App\Http\Controllers;

use App\Materi;
use App\MateriImages;
use App\Soal;
use App\SubMateri;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MateriController extends Controller
{
    public function index($id)
    {

        $materi = Materi::find($id);

        $sub_materi = SubMateri::with('materis')->where('materi_id', $id)->get();
        $materi_images = MateriImages::where('materi_id', $id)->get();


        return view('user.materi', compact('materi', 'sub_materi', 'materi_images', ));
    }

    public function soal(Request $request,$id)
    {
        $soal = Soal::with('materi')->where('materi_id', $id)->inRandomOrder()->get();
        $materi = Materi::find($id);

        return view('user.latihan', compact('soal', 'materi'))->with('i');
    }

    public function user()
    {
        $materi = Materi::get();

        return view('user.index', compact('materi'));
    }

//     public function score(Request $request)
//     {

//         $nilai = DB::table('materis')
//             ->join('soals', 'materis.id', '=', 'soals.materi_id')
//             ->where('materi_id', 1)
//             ->select('jawaban_benar')
//             ->get();
//             $score = 0;
//         foreach ($nilai as $key => $value) {
//             # code...
//         }
//         if ($request == $nilai)
//         {

//         }
//         return view('limit.latihan',compact('score'));
//     }
}

<?php

namespace App\Http\Controllers;

use App\Materi;
use App\MateriImages;
use Illuminate\Http\Request;

class MateriImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materi_images = MateriImages::with('materi_images')->paginate(10);


        return view('materi-images.index', compact('materi_images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $materis = Materi::all();
        return view('materi-images.create', compact('materis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'materi_id' => 'required',
            'image' => 'required'
        ]);


        $path = $request->file('image')->store('public/images');
        $materi_image = new MateriImages;
        $materi_image->materi_id = $request->materi_id;
        $materi_image->image = $path;
        $materi_image->save();


        return redirect()->route('materi-images.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MateriImages $materi_image)
    {
        $materis = Materi::all();
        return view('materi-images.edit', compact('materi_image', 'materis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'materi_id' => 'required',
        ]);

        $materi_image = MateriImages::find($id);
        if($request->hasFile('image')){
            $request->validate([
              'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $path = $request->file('image')->store('public/images');
            $materi_image->image = $path;
        }
        $materi_image->materi_id = $request->materi_id;
        $materi_image->save();

        return redirect()->route('materi-images.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MateriImages $materi_image)
    {
        $materi_image->delete();

        return redirect()->route('materi-images.index');
    }
}

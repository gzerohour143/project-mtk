<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Image;

class UserController extends Controller
{
    public function profile()
    {
        $nama = DB::table('users')->select('name')->substr(1,1);

        return view('profile', [
            'user' => Auth::user(),
            'nama' => $nama
        ]);
    }

    public function update_photo(Request $request)
    {
        if($request->hasFile('photo'))
        {
            $photo = $request->file('photo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            Image::make($photo)->resize(300, 300)->save(public_path('uploads/photo/' . $filename));

            $user = Auth::user();
            $user->photo = $filename;
            $user->save();
        }

        return view('home', [
            'user' => Auth::user()
        ]);
    }
}

@extends('admin.template.default')

@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Dashboard-->
            <!--begin::Row-->
            <div class="row">
                <div class="col-xl-12">
                    <div class="card card-custom">
                        <div class="card-header">
                        <h3 class="card-title">
                        Materi Images Inputs
                        </h3>
                        </div>
                        <!--begin::Form-->
                        <form action="{{ route('materi-images.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label  class="col-2 col-form-label">Nama Materi</label>
                                    <div class="col-10">
                                        <select name="materi_id" class="form-control">
                                            @foreach ($materis as $materi)
                                                <option value="{{ $materi->id }}">{{ $materi->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-2 col-form-label">Image</label>
                                    <div class="col-10">
                                        <input class="form-control" name="image" type="file" placeholder="Image" id="example-text-input"/>
                                    </div>
                            </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-2">
                                </div>
                                <div class="col-10">
                                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                                    <a href="{{ route('materi-images.index') }}" class="btn btn-secondary">Cancel</a>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

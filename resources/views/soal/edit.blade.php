@extends('admin.template.default')

@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Dashboard-->
            <!--begin::Row-->
            <div class="row">
                <div class="col-xl-12">
                    <div class="card card-custom">
                        <div class="card-header">
                        <h3 class="card-title">
                        Soal Inputs
                        </h3>
                        </div>
                        <!--begin::Form-->
                        <form action="{{ route('soal.update',$soal->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                        <div class="card-body">
                            <div class="form-group row">
                                <label  class="col-2 col-form-label">Nama Materi</label>
                                    <div class="col-10">
                                        <select name="materi_id" class="form-control">
                                            @foreach ($materis as $materi)
                                                <option value="{{ $materi->id }}">{{ $materi->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                            </div>
                        <div class="form-group row">
                        <label  class="col-2 col-form-label">Pertanyaan</label>
                        <div class="col-10">
                            <textarea class="form-control" name="pertanyaan" type="text" placeholder="Pertanyaan" id="ckeditor"/>{{ $soal->pertanyaan }}</textarea>
                        </div>
                        </div>
                        <div class="form-group row">
                        <label for="example-search-input" class="col-2 col-form-label">Jawaban A</label>
                        <div class="col-10">
                            <input class="form-control" name="a" value="{{ $soal->a }}" type="search" placeholder="Jawaban A" id="example-search-input"/>
                        </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-search-input" class="col-2 col-form-label">Jawaban B</label>
                            <div class="col-10">
                                <input class="form-control" name="b" value="{{ $soal->b }}" type="search" placeholder="Jawaban B" id="example-search-input"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-search-input" class="col-2 col-form-label">Jawaban C</label>
                            <div class="col-10">
                                <input class="form-control" name="c" value="{{ $soal->c }}" type="search" placeholder="Jawaban C" id="example-search-input"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-search-input" class="col-2 col-form-label">Jawaban D</label>
                            <div class="col-10">
                                <input class="form-control" name="d" value="{{ $soal->d }}" type="search" placeholder="Jawaban D" id="example-search-input"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-search-input" class="col-2 col-form-label">Jawaban E</label>
                            <div class="col-10">
                                <input class="form-control" name="e" value="{{ $soal->e }}" type="search" placeholder="Jawaban E" id="example-search-input"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-search-input" class="col-2 col-form-label">Ubah Jawaban<span class="text-danger">*</span></label>
                            <div class="col-10">
                                <select class="form-control" name="jawaban_benar" id="exampleSelect1">
                                    <option value="a">A</option>
                                    <option value="b">B</option>
                                    <option value="c">C</option>
                                    <option value="d">D</option>
                                    <option value="e">E</option>
                                </select>
                            </div>
                        </div>
                        {{-- <div class="form-group row">
                            <label  class="col-2 col-form-label">Image Baru</label>
                                <div class="col-10">
                                    <input class="form-control" name="image" type="file" placeholder="Image" id="example-text-input"/>
                                </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Image Lama</label>
                                <div class="col-10">
                                    <img src="{{ Storage::url($soal->image) }}" alt="" width="112px" />
                                </div>
                        </div> --}}
                        <div class="card-footer">
                        <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a href="{{ route('soal.index') }}" class="btn btn-secondary">Cancel</a>
                        </div>
                        </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('admin.template.default')

@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Dashboard-->
            <!--begin::Row-->
            <div class="row">
                <div class="col-xl-12">
                    <div class="card card-custom">
                        <div class="card-header">
                        <h3 class="card-title">
                        Soal Inputs
                        </h3>
                        </div>
                        <!--begin::Form-->
                        <form action="{{ route('soal.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label  class="col-2 col-form-label">Nama Materi</label>
                                    <div class="col-10">
                                        <select name="materi_id" class="form-control">
                                            @foreach ($materis as $materi)
                                                <option value="{{ $materi->id }}">{{ $materi->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-2 col-form-label">Pertanyaan</label>
                                <div class="col-10">
                                    <textarea class="form-control" id="ckeditor" name="pertanyaan" type="text" placeholder="Pertanyaan" id="example-text-input"/></textarea>
                                </div>
                            </div>
                        <div class="form-group row">
                        <label for="example-search-input" class="col-2 col-form-label">Jawaban A</label>
                        <div class="col-10">
                            <input class="form-control" name="a" type="search" placeholder="Jawaban A" id="example-search-input"/>
                        </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-search-input" class="col-2 col-form-label">Jawaban B</label>
                            <div class="col-10">
                                <input class="form-control" name="b" type="search" placeholder="Jawaban B" id="example-search-input"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-search-input" class="col-2 col-form-label">Jawaban C</label>
                            <div class="col-10">
                                <input class="form-control" name="c" type="search" placeholder="Jawaban C" id="example-search-input"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-search-input" class="col-2 col-form-label">Jawaban D</label>
                            <div class="col-10">
                                <input class="form-control" name="d" type="search" placeholder="Jawaban D" id="example-search-input"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-search-input" class="col-2 col-form-label">Jawaban E</label>
                            <div class="col-10">
                                <input class="form-control" name="e" type="search" placeholder="Jawaban e" id="example-search-input"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-search-input" class="col-2 col-form-label">Jawaban Benar <span class="text-danger">*</span></label>
                            <div class="col-10">
                                <select class="form-control" name="jawaban_benar" id="exampleSelect1">
                                    <option value="a">A</option>
                                    <option value="b">B</option>
                                    <option value="c">C</option>
                                    <option value="d">D</option>
                                    <option value="e">E</option>
                                </select>
                            </div>
                        </div>
                        <div class="card-footer">
                        <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a href="{{ route('soal.index') }}" class="btn btn-secondary">Cancel</a>
                        </div>
                        </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

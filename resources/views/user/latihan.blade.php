<!DOCTYPE html>
<html lang="en">

@include('user.partials.head')
<style>
    .pt-3 p{
        display: inline;
    }
</style>
<body style="background:rgb(0, 132, 255)">
    @include('user.partials.header')
    <div class="container" style="margin-top: 50px; margin-bottom:50px;">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <form action="{{Route('materi.score')}}" method="POST">
                        @csrf
                    <div class="card-body">
                        <h2 style="color: cornflowerblue;margin-bottom:15px;text-align:center;">Latihan Soal</h2>
                        @foreach ($soal as $soal)
                        <h5 class="pt-3">{{ ++$i }}. {!! $soal->pertanyaan !!} </h5>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="soal[{{$soal->id}}]" id="{{$soal->a}}"
                                value="a">
                            <label class="form-check-label" for="{{$soal->a}}">
                                {{ $soal->a }}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="soal[{{$soal->id}}]" id="{{$soal->b}}"
                                value="b">
                            <label class="form-check-label" for="{{$soal->b}}">
                                {{ $soal->b }}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="soal[{{$soal->id}}]" id="{{$soal->c}}"
                                value="c">
                            <label class="form-check-label" for="{{$soal->c}}">
                                {{ $soal->c }}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="soal[{{$soal->id}}]" id="{{$soal->d}}"
                                value="d">
                            <label class="form-check-label" for="{{$soal->d}}">
                                {{ $soal->d }}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="soal[{{$soal->id}}]" id="{{$soal->e}}"
                                value="e">
                            <label class="form-check-label" for="{{$soal->e}}">
                                {{ $soal->e }}
                            </label>
                        </div>
                        @endforeach
                        <br>
                        <button type="submit" class="btn btn-danger float-right mb-1" data-toggle="modal"
                            data-target="#modalTambahBarang" id="selesai">Selesai</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalTambahBarang" tabindex="-1" aria-labelledby="modalTambahBarang" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Horeee</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!--FORM TAMBAH BARANG-->
                    <form action="" method=" ">
                        <div class="form-group">
                            <h6 for="">Kamu telah mengerjakan soal dengan nilai :</h6>
                            <h3 style="color:rgb(48, 189, 255)">100</h3>
                        </div>
                        <a href="{{ url('/materi/latihan/'.$materi->id) }}" class="btn btn-danger" role="button" aria-pressed="true">Latihan Lagi</a>
                        <a href="{{ url('/') }}" class="btn btn-primary" role="button" aria-pressed="true">Selesai</a>
                    </form>
                    <!--END FORM TAMBAH BARANG-->
                </div>
            </div>
        </div>
    </div>
    <!-- Copyright -->
    @include('user.partials.footer')
    <!-- General JS Scripts -->
    @include('user.partials.scripts')

    <!-- Page Specific JS File -->
</body>

</html>
<script type="text/javascript">


    $(document).ready(function() {
        $("#selesais").click(function(e){
            e.preventDefault();
                var datastring = $("#err").serialize();
            $.ajax({
                url: "/materis/latihan",
                type:'POST',
                data: datastring,
                dataType: "json",
                success: function(data) {

                }
            });


        });

        function printErrorMsg (msg) {
            $(".print-error-msg").find("ul").html('');
            $(".print-error-msg").css('display','block');
            $.each( msg, function( key, value ) {
                $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
            });
        }
    });
</script>


